//Agradeço a DEUS pelo dom do conhecimento

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');
var image = require('gulp-image');
var del = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('bs-sync', function() {
    browserSync.init(['src/css/*.css', 'src/js/*.js', 'src/views/*.html', 'src/index.html', 'src/js/**/*.js'], {
      proxy: 'localhost:8080',
      browser: ['google-chrome'],
      // browser: ["firefox"],
      //reloadDelay: 1000,
      open: false
    });
});

gulp.task('apagarPublic', function() {
  return del('public/*');
});

gulp.task('compactarJS', function() {
  return gulp.src('src/js/**/*.js')
//      .pipe(uglify())
      .pipe(gulp.dest('public/js'))
      .pipe(reload({stream: true}));
});

gulp.task('compactarHTML', function() {
   return gulp.src('src/views/*.html')
        .pipe(minify())
        .pipe(gulp.dest('public/views'))
        .pipe(reload({stream: true}));
});

gulp.task('compactarHTMLHome', function() {
  gulp.src('main.html')
      .pipe(minify())
      .pipe(gulp.dest('.'));
  return gulp.src('index.html')
      .pipe(minify())
      .pipe(gulp.dest('.'))
      .pipe(reload({stream: true}));

});

gulp.task('compactarCSS', function() {
  return gulp.src('src/css/*.css')
             .pipe(minify())
             .pipe(gulp.dest('public/css'))
             .pipe(reload({stream: true}));
});

gulp.task('compactarImagem', function() {
  return gulp.src('src/img/**/*')
             .pipe(image())
             .pipe(gulp.dest('static'));
});

gulp.task('watch', function() {
   gulp.watch('index.html', ['compactarHTMLHome']);
   gulp.watch('main.html', ['compactarHTMLHome']);
   gulp.watch('src/views/*.html', ['compactarHTML']);
   gulp.watch('src/js/**/*.js', ['compactarJS']);
   gulp.watch('src/js/*.js', ['compactarJS']);
   gulp.watch('src/css/*.css', ['compactarCSS']);
});

gulp.task('default', ['compactarHTML']);
gulp.task('development', [ 'compactarJS', 'compactarCSS', 'compactarHTMLHome', 'compactarHTML', 'compactarImagem', 'watch', 'bs-sync']);
gulp.task('production', function() {
  runSequence('apagarPublic', 'development');
});
