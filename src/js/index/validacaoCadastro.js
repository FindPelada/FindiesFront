var senhaCadastro = document.getElementById("senha-cadastro")
, repetirSenha = document.getElementById("repetir-senha");

function validatePassword(){
  if(senhaCadastro.value != repetirSenha.value) {
    repetirSenha.setCustomValidity("Senhas diferentes!");
  } else {
    repetirSenha.setCustomValidity('');
  }
}

senhaCadastro.onchange = validatePassword;
repetirSenha.onkeyup = validatePassword;
