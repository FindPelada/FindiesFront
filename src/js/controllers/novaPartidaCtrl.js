function mascara(t, mask){
  var i = t.value.length;
  var saida = mask.substring(1,0);
  var texto = mask.substring(i);
  if (texto.substring(0,1) != saida){
    t.value += texto.substring(0,1);
  }
}

app.controller('novaPartidaCtrl',['$scope','$http', '$rootScope', '$location', function($scope, $http, $rootScope, $location) {
  $scope.titulo = "Nova Partida";
  $scope.itemSelecionado = "itemSelecionado";
  var localPartida = "Aeroclube natal rn";




  $scope.divSelecaoJogador = true;


  $http.get($rootScope.API + "jogadores")
    .then(function(response) {
        $scope.amigos = response.data;
        console.log("get $.amigos OK");
  });
  $http.get($rootScope.API + "times")
    .then(function(response) {
        $scope.times = response.data;
        console.log("get $.times OK");
  });

  $scope.selecionados = [];
  $scope.timesSelecionados = [];
  $scope.dadosEnviar = {
    visibilidade: "",
    local: "",
    frequencia: "Única",
    data: "",
    diasFrequencia: [],
    horario: "",
    jogadores: [],
    times: []
  };

  $scope.definirLocal = function(){
    $scope.dadosEnviar.local = localPartida;
  };


  $scope.mudarEstadoBotaoJogador = function(){
    $scope.divSelecaoJogador = true;
  };
  $scope.mudarEstadoBotaoTime = function(){
    $scope.divSelecaoJogador = false;
  };



  // MUDA O ESTADO SELECIONADOS
  $scope.mudarEstadoSelecionado2 = function(amigos, amigo){
    var index = amigos.indexOf(amigo);
    if (amigo.selecionado !== true) {
      $scope.selecionados[index].selecionado = true;
    }else{
      $scope.selecionados[index].selecionado = false;
    }
  };
  $scope.mudarEstadoSelecionado = function(amigos, amigo){
    var index = amigos.indexOf(amigo);
    if (amigo.selecionado !== true) {
      $scope.amigos[index].selecionado = true;
    }else{
      $scope.amigos[index].selecionado = false;
    }
  };

      // ADICIONAR JOGADOR À LISTA DE JOGADORES SELECIONADOS
  $scope.naoRetorna = function(amigo){
    return amigo !== undefined;
  };
  $scope.selecionarJogador = function(amigos){
      var temp = amigos.filter(function (amigo){
        if (amigo.selecionado){
          var indice = amigos.indexOf(amigo);
          delete $scope.amigos[indice];
          amigo.selecionado = false;
          return amigo;
        }
      });
      $scope.selecionados = $scope.selecionados.concat(temp);
  };

      // RETORNAR JOGADOR À LISTA DE AMIGOS (RETIRA DE SELECIONADOS)
  $scope.retornarJogador = function(selecionados){
      var temp = selecionados.filter(function (selecionado){
        if (selecionado.selecionado){
          var indice = selecionados.indexOf(selecionado);
          delete $scope.selecionados[indice];
          selecionado.selecionado = false;
          return selecionado;
        }
      });
      $scope.amigos = $scope.amigos.concat(temp);
  };
      // FIM RETORNA JOGADOR



  // TIME

  // MUDA O ESTADO SELECIONADOS
  $scope.mudarEstadoTimeSelecionado2 = function(times, time){
    var index = times.indexOf(time);
    if (time.selecionado !== true) {
      $scope.timesSelecionados[index].selecionado = true;
    }else{
      $scope.timesSelecionados[index].selecionado = false;
    }
  };
  $scope.mudarEstadoTimeSelecionado = function(times, time){
    var index = times.indexOf(time);
    if (time.selecionado !== true) {
      $scope.times[index].selecionado = true;
    }else{
      $scope.times[index].selecionado = false;
    }
  };

      // ADICIONAR TIME À LISTA DE JOGADORES SELECIONADOS
  $scope.selecionarTime = function(times){
      var temp = times.filter(function (time){
        if (time.selecionado){
          var indice = times.indexOf(time);
          delete $scope.times[indice];
          time.selecionado = false;
          return time;
        }
      });
      $scope.timesSelecionados = $scope.timesSelecionados.concat(temp);
  };

      // RETORNAR JOGADOR À LISTA DE AMIGOS (RETIRA DE SELECIONADOS)
  $scope.retornarTime = function(timesSelecionados){
          var temp = timesSelecionados.filter(function (timeSelecionado){
            if (timeSelecionado.selecionado){
              // console.log(timeSelecionado.selecionado);
              var indice = timesSelecionados.indexOf(timeSelecionado);
              delete $scope.timesSelecionados[indice];
              timeSelecionado.selecionado = false;
              return timeSelecionado;
            }
          });
          $scope.times = $scope.times.concat(temp);
      };

  console.log($rootScope.token);

  sendPost = function(dadosEnviar){
    var req = {
     method: 'POST',
     url: $rootScope.API + 'partidas/new',
     headers: {
      'Content-Type': 'application/json',
      'Token': $rootScope.token
     },
     data: dadosEnviar
   };
    $http(req).then(function successCallback(response){
      var temp = response.data;
      console.log(temp);
      // if (temp.statusPartida === true && temp.statusAdmPartida === true && temp.statusJogadoresPartida === true && temp.statusTimePartida === true) {
        console.log("Partida salva com sucesso!");
        $location.path('/');
      // }
    }, function errorCallback(response){
      console.log("Erro");
      console.log(response.data);
    });
  };



  // CRIAR PARTIDA
  $scope.criarPartida = function(x, y, dadosEnviar){
    jogadores = dadosEnviar.jogadores;
    times = dadosEnviar.times;
    for (var i = 0; i < x.length; i++) {
      jogadores.push(x[i].idJogador);
      $scope.dadosEnviar.jogadores = jogadores;
    }
    for (var j = 0; j < y.length; j++) {
      times.push(y[j].idTime);
      $scope.dadosEnviar.times = times;
    }
    sendPost(dadosEnviar);
  };





var currentTime = new Date();
$scope.currentTime = currentTime;
$scope.month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
$scope.monthShort = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
$scope.weekdaysFull = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
$scope.weekdaysLetter = ['D', 'S', 'T', 'Q', 'Q', 'S','S'];
$scope.disable = [false];
$scope.today = 'Hoje';
$scope.clear = 'Limpar';
$scope.close = 'Fechar';
var days = 92;
$scope.minDate = (new Date($scope.currentTime.getTime())).toISOString();
$scope.maxDate = (new Date($scope.currentTime.getTime() + ( 1000 * 60 * 60 *24 * days ))).toISOString();


console.log("novaPartidaCtrl carregado!");
}]);
