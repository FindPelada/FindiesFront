app.controller('perfilCtrl',['$scope', '$http', '$rootScope', function($scope, $http, $rootScope) {
  $scope.titulo = "Perfil";

  $http.get($rootScope.API + "jogadores/" + $rootScope.user.id)
    .then(function(response) {
        $scope.jogador = response.data[0];
        delete $scope.jogador.senha;
        delete $scope.jogador.foto;
        delete $scope.jogador.idJogador;
        delete $scope.jogador.data_vinculo;
        delete $scope.jogador.pesquisa;
        delete $scope.jogador.perfilvisualizado;
        delete $scope.jogador.idTimeVisualizado;
        delete $scope.jogador.idPeladaSelecionada;
        console.log("get $jogador by http is OK");
  });

  $scope.disabled = true;
  $scope.ableForm = function(){
    $scope.disabled = false;
  };
  $scope.save = function(jogadorUpdate){

    alert(jogadorUpdate);

    var req = {
       method: 'PUT',
       url: $rootScope.API + 'jogadores/' + $rootScope.user.id + "/update",
       headers: {
        'Content-Type': 'application/json',
        'Token': $rootScope.token
       },
       data: jogadorUpdate

    };
     console.log(req);
      $http(req).then
      (function(response){

        console.log(response.data);
        $scope.disabled = true;

      }, function(response){
        console.log(response);
        alert("não foi possivel atualizar os dados. Tente novamente.");
      });

  };

  console.log("perfilCtrl carregado!");
}]);
