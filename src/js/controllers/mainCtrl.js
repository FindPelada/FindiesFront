app.controller('mainCtrl',["$scope", '$localStorage', 'jwtHelper', '$rootScope', '$location', function($scope, $localStorage, jwtHelper, $rootScope, $location) {

  $rootScope.API = 'http://findies.esy.es/api/';
  // $rootScope.FRONT = 'http://findies-web.esy.es/';
  $rootScope.FRONT = 'http://localhost:3000/';
  $rootScope.API_STATIC = 'http://findies.esy.es/Findies/static/';
  $rootScope.user = {};
  var getToken = function () {
    $rootScope.token = $localStorage.token;
  };
  var deleteToken = function(){
    delete $localStorage.token;
  };
  var setToken = function (token) {
    $localStorage.token = token;
  };
  getToken();


  //  generalProcedures
  var generalProcedures = function(token){
    var x = jwtHelper.decodeToken(token);
    $rootScope.user = {
      id: x.id,
      nome: x.nome,
      email: x.useremail
    };
  };
  generalProcedures($rootScope.token);
  console.log($rootScope.user);

  // LOGOUT
  $scope.logout = function(){
    deleteToken();
    delete $localStorage.lastSearch;
    location.href = $rootScope.FRONT;
  };

  // BUSCA
  $scope.search = function(x){
    $rootScope.tempTextoBusca = x;
    $localStorage.lastSearch = x;
    $location.path('/busca');
  };


  console.log("mainCtrl carregado!");
}]);
