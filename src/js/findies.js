angular.module.exports = 'angular-jwt';
var app = angular.module('findies', ['ui.materialize', 'angular-jwt',"ngRoute", 'ngStorage']);
app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {

    $routeProvider
    .when("/", {
      templateUrl: "public/views/home.html",
      controller: "homeCtrl"
    })
    .when("/amigos", {
      templateUrl: "public/views/amigos.html",
      controller: "amigosCtrl"
    })
    .when("/novaPartida", {
      templateUrl: "public/views/novaPartida.html",
      controller: "novaPartidaCtrl"
    })
    .when("/times", {
      templateUrl: "public/views/times.html",
      controller: "timesCtrl"
    })
    .when("/novoTime", {
      templateUrl: "public/views/novoTime.html",
      controller: "novoTimeCtrl"
    })
    .when("/perfil", {
      templateUrl: "public/views/perfil.html",
      controller: "perfilCtrl"
    })
    .when("/busca", {
      templateUrl: "public/views/busca.html",
      controller: "buscaCtrl"
    })
    .otherwise({
      templateUrl: "public/views/home.html",
      controller: "homeCtrl"
    });
}]);
