app.controller('amigosCtrl',["$scope", "$http", '$rootScope', '$localStorage', function($scope, $http, $rootScope, $localStorage) {
  $scope.titulo = "Amigos";
  $http.get($rootScope.API + 'jogadores/'+$rootScope.user.id+'/amigos')
    .then(function(response) {
        $scope.jogadores = response.data;
  });

  $scope.naoRetorna = function(jogador){
    return jogador !== undefined;
  };

  $scope.removerAmigo = function(jogador, jogadores) {
    console.info(jogador);
    console.info($localStorage.token);
    console.info($rootScope.API + 'jogadores/amigos/'+jogador.idAmigo+'/delete');
    $http({
      method: 'DELETE',
      url: $rootScope.API + 'jogadores/amigos/'+jogador.idAmigo+'/delete',
      data : undefined,
      headers: {
        'Token': $localStorage.token
      }})
    .then(function successCallback(response) {
          var indice = jogadores.indexOf(jogador);
          delete $scope.jogadores[indice];
      }, function errorCallback(response) {
        console.info(response);
        window.alert('Erro');
      });
    };
  console.log("amigosCtrl carregado!");
}]);
