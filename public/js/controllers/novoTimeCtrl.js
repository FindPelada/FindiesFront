app.controller('novoTimeCtrl',['$scope','$http', '$location', '$rootScope', function($scope, $http, $location, $rootScope) {
  $scope.titulo = "Novo Time";
  $http.get($rootScope.API + 'jogadores')
    .then(function(response) {
        $scope.amigos = response.data;
  });
  $scope.sendPost = function(dadosEnviar, selecionados) {

  for (var i = 0; i < selecionados.length; i++) {
      dadosEnviar.amigos[i] = selecionados[i].idJogador;
  }
    console.log($rootScope.token);
  var req = {
     method: 'POST',
     url: $rootScope.API + 'times/new',
     headers: {
      'Content-Type': 'application/json',
      'Token': $rootScope.token
     },
     data: dadosEnviar
  };
   console.log(req);
    $http(req).then
    (function(response){
      var temp = response.data[0];
      if (temp.statusTime && temp.statusAdmTime && temp.statusAmigosTime) {
        $location.path('/times');
      }
      console.log(temp.statusTime);
    }, function(response){
      var temp = response.data[0];
      console.log(response.data);
    });

    console.log("POST ok");
  };

  $scope.selecionados = [];
  $scope.dadosEnviar = {
    nome: "",
    mascote: "",
    amigos: []
  };

  // VARIÁVEIS DO NG-CLASS
  $scope.amigoSelecionado = "amigoSelecionado";



  // $scope.amigos = [
  //   {nome_jogador: "Pedro",apelido_jogador: "nome", idJogador: "99998888", cor: "blue"},
  //   {nome_jogador: "Ana",apelido_jogador: "nome", idJogador: "99997777", cor: "yellow"},
  //   {nome_jogador: "Maria",apelido_jogador: "nome", idJogador: "99996666", cor: "red"}
  // ];


  // MUDA O ESTADO DO JOGADOR PARA "SELECIONADO=TRUE" QUANDO É CLICADO

  $scope.mudarEstadoSelecionado2 = function(amigos, amigo){
    var index = amigos.indexOf(amigo);
    if (!amigo.selecionado === true) {
      $scope.selecionados[index].selecionado = true;
    }else{
      $scope.selecionados[index].selecionado = false;
    }
  }
  $scope.mudarEstadoSelecionado = function(amigos, amigo){
    var index = amigos.indexOf(amigo);
    if (!amigo.selecionado === true) {
      $scope.amigos[index].selecionado = true;
    }else{
      $scope.amigos[index].selecionado = false;
    }
  }


      // ADICIONAR JOGADOR À LISTA DE JOGADORES SELECIONADOS
  $scope.naoRetorna = function(amigo){
    return amigo !== undefined;
  }
  $scope.selecionarJogador = function(amigos){
      var temp = amigos.filter(function (amigo){
        if (amigo.selecionado){
          var indice = amigos.indexOf(amigo);
          delete $scope.amigos[indice];
          amigo.selecionado = false;
          return amigo;
        }
      });
      $scope.selecionados = $scope.selecionados.concat(temp);
  };
      // FIM ADICIONAR JOGADOR

      // RETORNAR JOGADOR À LISTA DE AMIGOS (RETIRA DE SELECIONADOS)
  $scope.retornarJogador = function(selecionados){
      var temp = selecionados.filter(function (selecionado){
        if (selecionado.selecionado){
          var indice = selecionados.indexOf(selecionado);
          delete $scope.selecionados[indice];
          selecionado.selecionado = false;
          return selecionado;
        }
      });
      $scope.amigos = $scope.amigos.concat(temp);
  };
      // FIM RETORNA JOGADOR

      // VERIFICAR E ENVIAR DADOS
  $scope.enviarDados = function(selecionados, dadosEnviar){
    // adicionando jogador ao JSON dadosEnviar
    console.log("enviarDados ativado!");
    for (var i = 0; i < selecionados.length; i++) {
      dadosEnviar.selecionados["id" + i] = selecionados[i].idJogador;
    }
  };


  console.log("novoTimeCtrl carregado!");
}]);
