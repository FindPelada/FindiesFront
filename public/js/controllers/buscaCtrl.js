app.controller('buscaCtrl',['$scope', 'jwtHelper', '$http', '$rootScope', '$localStorage', function($scope, jwtHelper,  $http, $rootScope, $localStorage) {

  var isThere = localStorage.getItem('ngStorage-lastSearch');
  if (isThere === null) {
    location.href = 'main.html';
    $scope.textoBusca = $rootScope.tempTextoBusca;
  }else{
    $scope.textoBusca = $localStorage.lastSearch;
  }


  $scope.titulo = "Resultado de busca para '" + $scope.textoBusca + "'";
  $rootScope.tempTextoBusca = "";

  $scope.naoRetorna = function(amigo){
    return amigo !== undefined;
  };

  $http.get($rootScope.API + 'jogadores')
    .then(function(response) {
        $scope.jogadores = response.data;
  });
  $http.get($rootScope.API + "times")
    .then(function(response) {
        $scope.times = response.data;
        console.log("get $.times by http is OK");
  });

  console.log("buscaCtrl carregado!");
}]);
