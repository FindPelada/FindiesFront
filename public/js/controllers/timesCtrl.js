app.controller('timesCtrl',['$scope','$http', '$rootScope', function($scope, $http, $rootScope) {

  $scope.titulo = "Times";
  $scope.arrayJogadoresTimes = [];
  $scope.amigos;
  $scope.idTimeToEdit;

  // VARIÁVEIS DO NG-CLASS
  $scope.amigoSelecionado = "amigoSelecionado";

  // REQUISIÇÃO DOS TIMES EM QUEM O JOGADOR SE ENCONTRA
  $http.get($rootScope.API + "times/" + $rootScope.user.id)
    .then(function(response) {
        $scope.times = response.data;
        console.log("get $.times by http is OK");
  });



  // ============== NAO RETORNA OS VALORES NULL NO NG-REPEAT =======================
  // ESCONDE OS VALORES NULL
  $scope.naoRetorna = function(amigo){
    return amigo !== undefined;
  }
  // ESCONDE OS VALORES JÁ EXISTENTES
  $scope.naoRetornaExistente = function(amigo){
    var idTimeToEdit = $scope.idTimeToEdit;
    var jogadoresTime = $scope.arrayJogadoresTimes[idTimeToEdit];
    for (var jogador in jogadoresTime) {
      if (jogador.id !== amigo.id) {
        return amigo
      }
    }
  }
  // ===============================================================================


  // =============== VERIFICA SE "AMIGOS" JÁ FORAM CHAMADOS ========================
  $scope.getAmigos = function(amigos){
    if (!amigos) {
      $http.get($rootScope.API + "jogadores/" + $rootScope.user.id + "/amigos" )
        .then(function(response) {
            $scope.amigos = response.data;
            console.log("get $.amigos by http is OK");
      });
    }
  }
  // ===============================================================================


  // ============== HTTP.GET PARA OS JOGADORES DE CADA TIME =======================
  // CAPTURA OS JOGADORES DE CADA TIME EM QUE O JOGADOR ESTIVER
  $scope.getJogadoresTime = function(arrayJogadoresTimes, idTime){
    var varIdTime = idTime;
    // FAZ A REQUISIÇÃO APENAS SE ELA NUNCA TIVER SIDO FEITA ANTERIORMENTE
    if (!arrayJogadoresTimes[varIdTime]) {
      $http.get($rootScope.API + "times/" + varIdTime + "/jogadores")
      .then(function(response) {
        $scope.arrayJogadoresTimes[varIdTime] = response.data;
      });
    }
    $scope.tempArrayJogadoresTimes = angular.copy($scope.arrayJogadoresTimes);
  }
// ==============================================================================

// ============================= SAIR DO TIME ===================================
  // RETORNA O INDICE DO TIME PARA QUE POSSA SER DELETADO
  $scope.getIndexTime = function(times, time){
    var index = times.indexOf(time);
    $scope.indexTime = index;
  }
  // DELETAR TIME DA LISTA DE TIMES
  $scope.deleteTime = function(indexTime, times){
    // AKI VEM O POST PARA SAIR DO TIME
    var req = {
       method: 'POST',
       url: $rootScope.API + 'times/sair',
       headers: {
        'Content-Type': 'application/json',
        'Token': $rootScope.token
       },
       data: {idJogador: $rootScope.user.id, idTime: times[indexTime].idTime}

    };
     console.log(req);
      $http(req).then
      (function(response){

        console.log(response.data);
        delete times[indexTime];
        $scope.times = times;


      }, function(response){
        console.log(response);
      });

  }
// ==============================================================================

// ======================= REMOVER JOGADOR DO TIME ==============================
  $scope.tempArrayJogadoresTimes = angular.copy($scope.arrayJogadoresTimes);

  $scope.cancelarRemocaoJogadorTime = function(){
    $scope.tempArrayJogadoresTimes = angular.copy($scope.arrayJogadoresTimes);
  }
  $scope.salvarRemocaoJogadorTime = function(){
    $scope.arrayJogadoresTimes = $scope.tempArrayJogadoresTimes;
  }
  $scope.setIdTimeToEdit = function(idTime){
    $scope.idTimeToEdit = idTime;
  }
  $scope.deleteJogador = function(tempArrayJogadoresTimes, jogador, idTimeToEdit){
    var index = tempArrayJogadoresTimes.indexOf(jogador);
    delete tempArrayJogadoresTimes[index];
  }
// ==============================================================================

// ===================== ADICIONAR JOGADORES AO TIME ============================
  $scope.mudarEstadoSelecionado = function(amigos, amigo){
    var index = amigos.indexOf(amigo);
    if (!amigo.selecionado === true) {
      $scope.amigos[index].selecionado = true;
    }else{
      $scope.amigos[index].selecionado = false;
    }
  }
  $scope.adicionarJogador = function(amigos, arrayJogadoresTimes, idTimeToEdit){
      var temp = amigos.filter(function (amigo){
        if (amigo.selecionado){
          var indice = amigos.indexOf(amigo);
          amigo.selecionado = false;
          return amigo;
        }
      });
      $scope.arrayJogadoresTimes[idTimeToEdit] = $scope.arrayJogadoresTimes[idTimeToEdit].concat(temp);
  };
// ==============================================================================

  console.log("timesCtrl carregado");
}]);
