angular.module.exports = 'angular-jwt';
var app = angular.module('findies', ['ui.materialize', 'angular-jwt', 'ngStorage']);

app.controller('indexCtrl',['$scope', '$http', '$timeout', '$localStorage', 'jwtHelper', '$rootScope', function($scope, $http, $timeout, $localStorage, jwtHelper, $rootScope) {

  $rootScope.API = 'http://findies.esy.es/api/';
  // $rootScope.FRONT = 'http://findies-web.esy.es/';
  $rootScope.FRONT = 'http://localhost:3000/';
  $rootScope.API_IMAGEM = 'http://findies.esy.es/Findies/static/';

  $scope.divError = false;
  $scope.tempToken = null;
  // $scope.token = null;
  $scope.dadosEnviarLogin = {
    emailLogin: "",
    senhaLogin: ""
  };
  $scope.dadosEnviarCadastrar = {
  	nome:"",
  	sobrenome:"",
  	date: "",
  	cep:"",
  	emailcadastro:"",
  	senhacadastro: ""
  };

  var getToken = function () {
    $scope.token = $localStorage.token;
  };
  var deleteToken = function(){
    delete $localStorage.token;
  };
  var setToken = function (token) {
    $localStorage.token = token;
  };

  $scope.getAmigos = function(token) {
    var req = {
     method: 'GET',
     url: $rootScope.API + 'jogadores',
     headers: {
       'Content-Type': 'application/json',
       'token': token
     }
   };
    $http(req).then(function(response){
      $scope.amigos = response.data;
    });
  };

  $scope.sendPostLogin = function(dadosEnviarLogin) {
    var req = {
       method: 'POST',
       url: $rootScope.API + 'jogadores/login',
       headers: {
         'Content-Type': 'application/json'
       },
       data: dadosEnviarLogin
    };
    $http(req).then(function(response){
      x = response.data;
      if (x.statusEmailouSenha === false) {
        $scope.divError = true;
        $scope.messageError = "Dados incorretos";
      }else {
        if (x.token !== null) {
          $scope.token = x.token;
          $scope.tokenDecoded = jwtHelper.decodeToken(x.token);
          setToken(x.token);
          location.href = $rootScope.FRONT + 'main.html';
          console.log(response.statusText);
        }
      }
    });
  };

  $scope.sendPostCadastrar = function(dadosEnviarCadastrar) {
    $scope.isEmail = "";
    $scope.isEmailExiste = "";
    $http.get($rootScope.API + "email/" + dadosEnviarCadastrar.emailcadastro)
      .then(function(response) {
          $scope.isEmail = response.data;
    });
    $scope.isEmailExiste = $scope.isEmail.existe;
    console.log($scope.isEmailExiste);
    if ($scope.isEmailExiste) {
      console.log("cima: " + $scope.isEmailExiste);
      // $scope.divError = true;
      // $scope.messaError = "Email já cadastrado!";
      console.log("Email já cadastrado!");
    }else {
      console.log("baixo: " + $scope.isEmailExiste);
      var req = {
         method: 'POST',
         url: $rootScope.API + 'jogadores/new',
         headers: {
           'Content-Type': 'application/json'
         },
         data: dadosEnviarCadastrar
      };
      $http(req).then(function(response){
          console.log(response.statusText);
          // x = response.data;
          // if (x.token !== null) {
          //   $scope.token = x.token;
          //   console.log(response.statusText);
          // }else {
          //   if (!x.statusEmail) {
          //     console.log("Email ou senha incorretos");
          //   }
          // }
      });
    }
  };

  // var timeP = function(){

    var currentTime = new Date();
    $scope.currentTime = currentTime;
    $scope.month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    $scope.monthShort = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
    $scope.weekdaysFull = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
    $scope.weekdaysLetter = ['D', 'S', 'T', 'Q', 'Q', 'S','S'];
    $scope.disable = [false];
    $scope.today = 'Hoje';
    $scope.clear = 'Limpar';
    $scope.close = 'Fechar';
    $scope.minDate = (new Date($scope.currentTime.getTime())).toISOString();
    $scope.maxDate = (new Date($scope.currentTime.getTime())).toISOString();
  // };
  // timeP();

}]);
